package config

import (
	"bytes"
	"os"
	"regexp"
	"strings"
)

var (
	envRegex        = regexp.MustCompile(`\${[0-9A-Za-z_.]+(:((\${[^}]+})|[^}])+)?}`)
	escapedEnvRegex = regexp.MustCompile(`\${({[0-9A-Za-z_.]+(:((\${[^}]+})|[^}])+)?})}`)
)

// ReplaceEnvVariables
//
// Magic from https://github.com/benthosdev/benthos/blob/v4.4.1/internal/config/env_vars.go
// Fork because we can't use its internal package.
func ReplaceEnvVariables(inBytes []byte) []byte {
	replaced := envRegex.ReplaceAllFunc(
		inBytes,
		func(content []byte) []byte {
			var value string
			if len(content) > 3 {
				if colonIndex := bytes.IndexByte(content, ':'); colonIndex == -1 {
					value = os.Getenv(string(content[2 : len(content)-1]))
				} else {
					targetVar := content[2:colonIndex]
					defaultVal := content[colonIndex+1 : len(content)-1]

					value = os.Getenv(string(targetVar))
					if value == "" {
						value = string(defaultVal)
					}
				}
				// Escape newlines, otherwise there's no way that they would work
				// within a config.
				value = strings.ReplaceAll(value, "\n", "\\n")
			}
			return []byte(value)
		},
	)

	return escapedEnvRegex.ReplaceAll(replaced, []byte("$$$1"))
}
