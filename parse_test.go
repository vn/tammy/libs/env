package config

import (
	"fmt"
	"os"
	"testing"
)

func TestConfig(t *testing.T) {
	os.Setenv("FOO", "FUZZZ")
	raw := []byte(`{"foo":"${FOO}", "bar": "${BAR:bazzz}"}`)
	
	raw = ReplaceEnvVariables(raw)
	
	fmt.Println("RAW: ", string(raw))
}
